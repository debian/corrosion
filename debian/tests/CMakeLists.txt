cmake_minimum_required(VERSION 3.15)
project(Corrosion
    # Official releases will be major.minor.patch. When the `tweak` field is
    # set it indicates that we are on a commit, that is not a officially
    # tagged release. Users don't need to care about this, it is mainly to
    # clearly see in configure logs which version was used, without needing to
    # rely on `git`, since Corrosion may be installed or otherwise packaged.
    VERSION 0.4.7
    LANGUAGES NONE
    HOMEPAGE_URL "https://corrosion-rs.github.io/corrosion/"
)
set(_CORROSION_TOP_LEVEL ON)
option(
    CORROSION_BUILD_TESTS
    "Build Corrosion test project"
    ${_CORROSION_TOP_LEVEL}
)

if (_CORROSION_TOP_LEVEL)
    # We need to enable a language for corrosions test to work.
    # For projects using corrosion this is not needed
    enable_language(C)
endif()

# Find system installed corrosion package
find_package(Corrosion REQUIRED)

# Testing
if (CORROSION_BUILD_TESTS)
    include(CTest)
    add_subdirectory(test)
endif()
